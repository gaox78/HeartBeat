package com.andaily.infrastructure.hibernate.queryhelper.impl;

import com.andaily.domain.application.ApplicationInstance;
import com.andaily.domain.shared.security.SecurityUtils;
import com.andaily.domain.user.User;
import com.andaily.infrastructure.hibernate.queryhelper.AbstractQueryHelper;
import com.andaily.infrastructure.hibernate.queryhelper.Filter;
import com.andaily.infrastructure.hibernate.queryhelper.ParameterFilter;
import com.andaily.infrastructure.hibernate.queryhelper.SortCriterionFilter;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.Map;

/**
 * 15-1-3
 *
 * @author Shengzhao Li
 */
public class ApplicationInstanceListQueryHelper extends AbstractQueryHelper<ApplicationInstance> {

    private Map<String, Object> map;

    public ApplicationInstanceListQueryHelper(Session session, Map<String, Object> map) {
        super(session);
        this.map = map;

        addInstanceNameFilter();
        addRegisterUserFilter();
        addEnabledFilter();
        addEmailFilter();

        addPrivateInstanceFilter();
        addSortFilter();
    }

    private void addEnabledFilter() {
        final String enabled = (String) map.get("enabled");
        if (StringUtils.isNotEmpty(enabled)) {
            addFilter(new ParameterFilter() {
                @Override
                public void setParameter(Query query) {
                    query.setParameter("enabled", "true".equalsIgnoreCase(enabled));
                }

                @Override
                public String getSubHql() {
                    return " and ai.enabled = :enabled ";
                }
            });
        }
    }

    private void addEmailFilter() {
        final String email = (String) map.get("email");
        if (StringUtils.isNotEmpty(email)) {
            addFilter(new ParameterFilter() {
                @Override
                public void setParameter(Query query) {
                    query.setParameter("email", "%" + email + "%");
                }

                @Override
                public String getSubHql() {
                    return " and ai.email like :email ";
                }
            });
        }
    }


    private void addPrivateInstanceFilter() {
        addFilter(new Filter() {
            @Override
            public String getSubHql() {
                if (SecurityUtils.currentUser() == null) {
                    return " and ai.privateInstance = false ";
                } else {
                    return "";
                }
            }
        });
    }


    private void addRegisterUserFilter() {
        final User user = SecurityUtils.currentUser();
        if (user != null && user.registerUser()) {
            addFilter(new ParameterFilter() {
                @Override
                public void setParameter(Query query) {
                    query.setParameter("user", user);
                }

                @Override
                public String getSubHql() {
                    return " and ai.creator = :user ";
                }
            });
        }
    }

    private void addSortFilter() {
        addSortCriterionFilter(new SortCriterionFilter() {
            @Override
            public String getSubHql() {
                return " ai.createTime desc ";
            }
        });
    }

    private void addInstanceNameFilter() {
        final String instanceName = (String) map.get("instanceName");
        if (StringUtils.isNotEmpty(instanceName)) {
            addFilter(new ParameterFilter() {
                @Override
                public void setParameter(Query query) {
                    query.setParameter("instanceName", "%" + instanceName + "%");
                }

                @Override
                public String getSubHql() {
                    return " and ai.instanceName like :instanceName ";
                }
            });
        }
    }


    @Override
    public int getStartPosition() {
        return (Integer) map.get("startIndex");
    }

    @Override
    public int getItemsAmountPerPage() {
        return (Integer) map.get("perPageSize");
    }

    @Override
    public String getAmountHql() {
        return " select count(ai.id) from ApplicationInstance ai where ai.archived = false ";
    }

    @Override
    public String getResultHql() {
        return " from ApplicationInstance ai where ai.archived = false ";
    }
}
